package sbu.cs.multithread.pi;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class DeductionCal {

    public static BigDecimal bigDecimal = new BigDecimal(0, new MathContext(1100, RoundingMode.HALF_DOWN));

    synchronized public static void sum(BigDecimal j) {
        bigDecimal = bigDecimal.add(j);
    }

    public static BigDecimal getBigDecimal() {
        return bigDecimal.multiply(new BigDecimal(2)).add(new BigDecimal(2)).setScale(1100,RoundingMode.HALF_DOWN);
    }

}
