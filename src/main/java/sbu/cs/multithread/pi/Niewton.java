package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.CountDownLatch;

public class Niewton implements Runnable {

    private int sentenceNum;
    private static BigDecimal result;

    public int getSentenceNum() {
        return sentenceNum;
    }

    public void setSentenceNum(int sentenceNum) {
        this.sentenceNum = sentenceNum;
    }

    @Override
    public void run() {
        result = evalNume(getSentenceNum()).divide(evalDec(getSentenceNum()),new MathContext(1100,RoundingMode.HALF_DOWN));
        DeductionCal.sum(result);
        PICalculator.countDownLatch.countDown();
    }

   synchronized public static BigDecimal evalDec(int sentenceNum){
       return new BigDecimal(String.valueOf((factorial(2*sentenceNum+1))));
    }

   synchronized public static BigDecimal evalNume(int sentenceNum){
        BigDecimal numenator = new BigDecimal(2);
        numenator = numenator.pow(sentenceNum);
        numenator = numenator.multiply(factorial(sentenceNum).pow(2));
        return numenator ;
    }

    synchronized public static BigDecimal factorial(int sentenceNum ){
        BigDecimal denaminator = new BigDecimal(1);
        for (int i = 1 ; i <= sentenceNum ; i++){
            BigDecimal temp = new BigDecimal(i);
            denaminator = denaminator.multiply(temp);
        }
        return denaminator;
    }
}