package sbu.cs.multithread.pi;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PICalculator {

    private static final ExecutorService executor = Executors.newFixedThreadPool(5);
    public static CountDownLatch countDownLatch;
    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public static String calculate(int floatingPoint) throws InterruptedException {

        countDownLatch = new CountDownLatch(1000);
        for (int i = 1 ; i <= 1000 ; ++i){
            Niewton niewton = new Niewton();
            niewton.setSentenceNum(i);
            executor.submit(niewton);
        }
        executor.shutdown();
        countDownLatch.await();
        return DeductionCal.getBigDecimal().toString().substring(0,floatingPoint+2);
    }



}

