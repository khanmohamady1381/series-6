package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Chef extends Thread {

    public Chef(String name) {
        super(name);
    }
    static Semaphore chefSem = new Semaphore(2);


    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {chefSem.acquire();

                Source.getSource();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            chefSem.release();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {

                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
