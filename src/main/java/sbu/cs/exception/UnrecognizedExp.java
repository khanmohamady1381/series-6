package sbu.cs.exception;

public class UnrecognizedExp extends ApException {
    public UnrecognizedExp(String Message) {
        super(Message);
    }
}
