package sbu.cs.exception;

import java.util.Arrays;
import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */

    public static void main(String[] args) throws ApException {
        readTwitterCommands(Util.getImplementedCommands());
    }

    public static void readTwitterCommands(List<String> args) throws ApException {
//
        for (int i = 0 ; i  < args.size() ; i++){
                if (Util.getNotImplementedCommands().contains(args.get(i))) {
                    throw new NotImplementExp("NotImplement Comment!!");
                }else {
                    if (!Util.getImplementedCommands().contains(args.get(i))&&!Util.getNotImplementedCommands().contains(args.get(i)))
                        throw new UnrecognizedExp("");
                }
//                }
//        if (Util.getImplementedCommands().contains("unfollow")){
//            System.out.println("Ys");
//        }
//        for (int i = 0 ; i < Util.getImplementedCommands().size() ;i++){
//            if (Util.getImplementedCommands().equals("unfollow")){
//                System.out.println("yes");
//            }else {
//                System.out.println("No");
//            }
//
        }
    }




    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public static void read(String...args) throws badInput {

        for (int i = 1 ; i < args.length ; i+=2){
            try{
                Integer.parseInt(args[i]);
            }catch (Exception e){
                throw new badInput("Bad Input");
            }
        }

    }


}
